<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        News::insert([
                'name'    => 'Good Investment',
                'tag_id'    => '2|4|8|9',
                'status' => 'draft',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);

        News::create([
                'name'    => 'Best Way Investment',
                'tag_id'    => '4|5|7|8',
                'status' => 'draft',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);

        News::create([
                'name'    => 'Crypto Investment',
                'tag_id'    => '2|3|5|7',
                'status' => 'deleted',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);

        News::create([
                'name'    => 'Crypto Investment Part 2',
                'tag_id'    => '2|3|4|7',
                'status' => 'deleted',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
