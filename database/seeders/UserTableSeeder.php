<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                'name'    => 'admin',
                'email'    => 'admin@test.com',
                'password'    => '$2a$12$cNi/GaUifq0FUOzmKCShauCI6OGD.VyvW7qNpcvBAMHuKRWfX.wdG',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
