<?php

return [
	'title' => 'Linbox',
	'NUMBER_PREFIX' => env('NUMBER_PREFIX','LBX-QUO'),
	'PRODUCT_PREFIX' => env('PRODUCT_PREFIX','LBX-PRD'),
	'MODE' => env('MODE', 'dev'),
];