<?php

use App\Http\Controllers\NewsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 

// Auth::routes();
Route::get('/', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/', [LoginController::class, 'login'])->name('login');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', [PagesController::class, 'Dashboard'])->name('dashboard');

    // Route::get('/csv-download', [PagesController::class, 'CSVDownload'])->name('csv-download');
    Route::get('/csv-download-new', [PagesController::class, 'CSVDownloadNew'])->name('csv-download-new');
    Route::get('/csv-download-jk1', [PagesController::class, 'CSVDownloadNew'])->name('csv-download-jk1');
    Route::get('/csv-download-jk2', [PagesController::class, 'CSVDownloadNew'])->name('csv-download-jk2');
    Route::get('/csv-preview-new/{id}', [InvoicesController::class, 'CSVPreviewNew'])->name('csv-preview-new');

    Route::get('/csv-preview-jk1/{id}', [InvoicesController::class, 'CSVPreviewNew'])->name('csv-preview-jk1');
    Route::get('/csv-preview-jk2/{id}', [InvoicesController::class, 'CSVPreviewNew'])->name('csv-preview-jk2');

    Route::get('list', [InvoicesController::class, 'InvoicesList']);
    Route::post('export-invoice', [InvoicesController::class, 'exportInvoices']);

    Route::get('invoicelist', [InvoicesController::class, 'InvoiceDataList']);

    Route::get('csv-export', [InvoicesController::class, 'ExportCsv'])->name('csv-export');
    Route::post('csv-export-new', [InvoicesController::class, 'ExportCsv'])->name('csv-export-new');

//News    
Route::get('/news', [NewsController::class, 'news'])->name('news');
Route::get('/news-add', [NewsController::class, 'news_add'])->name('news-add');
Route::post('/news-add', [NewsController::class, 'news_store'])->name('news-add');
Route::get('/news-edit/{id}', [NewsController::class, 'news_edit'])->name('news-edit');
Route::post('/news-edit/{id}', [NewsController::class, 'news_store_edit'])->name('news-edit');
Route::get('/news-delete/{id}', [NewsController::class, 'news_delete'])->name('news-delete');

//Tags    
Route::get('/tags', [TagsController::class, 'tags'])->name('tags');
Route::get('/tags-add', [TagsController::class, 'tags_add'])->name('tags-add');
Route::post('/tags-add', [TagsController::class, 'tags_store'])->name('tags-add');
Route::get('/tags-edit/{id}', [TagsController::class, 'tags_edit'])->name('tags-edit');
Route::post('/tags-edit/{id}', [TagsController::class, 'tags_store_edit'])->name('tags-edit');
Route::get('/tags-delete/{id}', [TagsController::class, 'tags_delete'])->name('tags-delete');

//Topic    
Route::get('/topic', [TopicController::class, 'topic'])->name('topic');
Route::get('/topic-add', [TopicController::class, 'topic_add'])->name('topic-add');
Route::post('/topic-add', [TopicController::class, 'topic_store'])->name('topic-add');
Route::get('/topic-edit/{id}', [TopicController::class, 'topic_edit'])->name('topic-edit');
Route::post('/topic-edit/{id}', [TopicController::class, 'topic_store_edit'])->name('topic-edit');
Route::get('/topic-delete/{id}', [TopicController::class, 'topic_delete'])->name('topic-delete');


//user
Route::get('/user', [UserController::class, 'user'])->name('user');
Route::get('/user-add', [UserController::class, 'user_add'])->name('user-add');
Route::post('/user-add', [UserController::class, 'user_store'])->name('user-add');
Route::get('/user-edit/{id}', [UserController::class, 'user_edit'])->name('user-edit');
Route::post('/user-edit/{id}', [UserController::class, 'user_store_edit'])->name('user-edit');
Route::get('/user-delete/{id}', [UserController::class, 'user_delete'])->name('user-delete');


// Route::get('/home', [HomeController::class, 'index'])->name('home');
});