@extends('layouts.master')
@section('title', 'Topic')
@section('page-title', 'Master Topic')
@section('breadcrumb', 'Master Topic')

@php
use App\Models\News;
@endphp

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
                <h3 class="card-title">List Topic</h3>
                    <a href="/topic-add"><i class="nav-icon fas fa-plus" style="float: right">ADD</i></a>
                
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Tag</th>
                        <th width="120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($topic) && $topic->count())
                        @foreach($topic as $key => $value)
                            @php
                                $news = new News();
                                $tagres = $news->getNewsName($value->news_id);
                            @endphp
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>{{ $tagres }}</td>
                                <td>
                                    <!-- <button class="btn btn-success" name="topic_id" value="{{ $value->topic_id }}"><i class="nav-icon far fa-edit"></i></button> -->
                                    <a href="/topic-edit/{{ $value->id }}"><i class="nav-icon btn-success fas fa-edit" style="padding: 5px;"></i></a>
                                    <a href="/topic-delete/{{ $value->id }}" onclick="return confirm('Are you sure?')"><i class="nav-icon btn-danger fa fa-trash" style="padding: 5px;"></i></a>
                                    <!-- <button class="btn btn-danger" name="topic_id" value="{{ $value->topic_id }}"><i class="nav-icon fa fa-trash"></i></button> -->
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $topic->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection