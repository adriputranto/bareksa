@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Master User')
@section('breadcrumb', 'Master User')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                @can('download')
                <h3 class="card-title">List User</h3>
                    <a href="/user-add"><i class="nav-icon fas fa-plus" style="float: right">ADD</i></a>
                @endcan
              </div>
        <form action="/" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th width="120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($user) && $user->count())
                        @foreach($user as $key => $value)
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->role }}</td>
                                <td>
                                    <!-- <button class="btn btn-success" name="reseller_id" value="{{ $value->reseller_id }}"><i class="nav-icon far fa-edit"></i></button> -->
                                    <a href="/user-edit/{{ $value->id }}"><i class="nav-icon btn-success fas fa-edit" style="padding: 5px;"></i></a>
                                    <a href="/user-delete/{{ $value->id }}" onclick="return confirm('Are you sure?')"><i class="nav-icon btn-danger fa fa-trash" style="padding: 5px;"></i></a>
                                    <!-- <button class="btn btn-danger" name="reseller_id" value="{{ $value->reseller_id }}"><i class="nav-icon fa fa-trash"></i></button> -->
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $user->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection