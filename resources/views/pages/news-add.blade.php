@extends('layouts.master')
@section('title', 'News')
@section('page-title', 'Master News')
@section('breadcrumb', 'Master News')

@section('content')

@php
if($is_edit){
    $disabled = 'disabled';
    $name = $res_edit->name;
    $stat = $res_edit->status;
    $tag = explode('|', $res_edit->tag_id);
    $action = '/news-edit/'.$id;
    $id = $id;
}else{
    $disabled = '';
    $name = '';
    $stat = '';
    $tag = array();
    $action = '/news-add';
    $id = '';
}
@endphp
    <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add News</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ $action }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        @if(!empty($id))
                            <input hidden="" value="{{ $id }}" name="id">
                        @endif
                        
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"  autocomplete="off" value="{{ $name }}">
                    </div>
                    <div class="form-group">
                        <label><strong>Select Tag :</strong></label><br/>
                                                        <select class="form-control" name="tag[]" multiple="">
                                                        @foreach($tags as $val)
                                                            @php
                                                            if (in_array($val->id, $tag)) {
                                                                $x = 'selected';
                                                            }else{
                                                                $x='';
                                                            }
                                                            @endphp
                                                          <option {{ $x }} value="{{ $val->id }}">{{ $val->name }}</option>
                                                        @endforeach  
                                                        </select>
                    </div>
                    <div class="form-group">
                        <label><strong>Select Status :</strong></label><br/>
                        <select name="status">
                            <option value="draft">draft</option>
                            <option value="deleted">deleted</option>
                            <option value="published">published</option>
                        </select>
                    </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            

          </div>
@endsection