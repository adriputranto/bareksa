@extends('layouts.master')
@section('title', 'Tags')
@section('page-title', 'Master Tags')
@section('breadcrumb', 'Master Tags')

@section('content')

@php
if($is_edit){
    $disabled = 'disabled';
    $name = $res_edit->name;
    $action = '/tags-edit/'.$id;
    $id = $id;
}else{
    $disabled = '';
    $name = '';
    $action = '/tags-add';
    $id = '';
}
@endphp
    <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Tags</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ $action }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        @if(!empty($id))
                            <input hidden="" value="{{ $id }}" name="id">
                        @endif
                        
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"  autocomplete="off" value="{{ $name }}">
                    </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            

          </div>
@endsection