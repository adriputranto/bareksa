@extends('layouts.master')
@section('title', 'News')
@section('page-title', 'Master News')
@section('breadcrumb', 'Master News')

@php
use App\Models\Tags;
@endphp

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
                <h3 class="card-title">List News</h3>
                    <a href="/news-add"><i class="nav-icon fas fa-plus" style="float: right">ADD</i></a>
                    <form action="">
                    <div class="w-4 float-left">
                        <!-- <input type="text" name="q" placeholder="Find or Search...!" class=""/> -->
                        <select name="q">
                            <option value="draft">draft</option>
                            <option value="deleted">deleted</option>
                            <option value="published">published</option>
                        </select>    
                        <input type="submit" class="btn btn-primary" value="Search"/>
                    </div>
                </form>
                
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Tag</th>
                        <th>Status</th>
                        <th width="120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($news) && $news->count())
                        @foreach($news as $key => $value)
                            @php
                                $tags = new Tags();
                                $tagres = $tags->getTagName($value->tag_id);
                            @endphp
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>{{ $tagres }}</td>
                                <td>{{ $value->status }}</td>
                                <td>
                                    <!-- <button class="btn btn-success" name="news_id" value="{{ $value->news_id }}"><i class="nav-icon far fa-edit"></i></button> -->
                                    <a href="/news-edit/{{ $value->id }}"><i class="nav-icon btn-success fas fa-edit" style="padding: 5px;"></i></a>
                                    <a href="/news-delete/{{ $value->id }}" onclick="return confirm('Are you sure?')"><i class="nav-icon btn-danger fa fa-trash" style="padding: 5px;"></i></a>
                                    <!-- <button class="btn btn-danger" name="news_id" value="{{ $value->news_id }}"><i class="nav-icon fa fa-trash"></i></button> -->
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $news->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection