@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Master User')
@section('breadcrumb', 'Master User')

@section('content')

@php
if($is_edit){
    $disabled = 'disabled';
    $name = $res_edit->name;
    $email = $res_edit->email;
    $password = '';
    $role = $res_edit->role;
    $action = '/user-edit/'.$id;
    $id = $id;
}else{
    $disabled = '';
    $name = '';
    $email = '';
    $password = '';
    $role = '';
    $action = '/user-add';
    $id = '';
}
@endphp
    <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ $action }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        @if(!empty($id))
                            <input hidden="" value="{{ $id }}" name="id">
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"  autocomplete="off" value="{{ $name }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ $email }}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" class="form-control" id="password" name="password" value="{{ $password }}">
                    </div>
                    <div class="form-group">
                        <label>Select Role</label>
                        <select class="form-control" name="role">
                          <option {{ $role=='admin'?'selected':'' }} value="admin">Admin</option>
                          <option {{ $role=='user'?'selected':'' }} value="user">User</option>
                        </select>
                    </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            

          </div>
<script>
    $(function () {
                $('.datetimepicker').daterangepicker({
                    singleDatePicker: true
                });
            });

    function getPricePlanAcronis(id) {
        $.getJSON('/acronis-pricing/'+id, null,
                    function(data){
                        // $('#body_item').innerHTML(data.result);
                        document.getElementById("body_item").innerHTML = data.result;
                        // alert(data.result);
                });
    }

    function sums(key){
        var price = $('#pricing'+key).val();
        var usage = $('#usage'+key).val();
        var total = parseFloat(price) * parseFloat(usage);
        $('#total'+key).val(parseFloat(total));
    }
</script>
@endsection