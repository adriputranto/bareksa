@extends('layouts.master')
@section('title', 'Tags')
@section('page-title', 'Master Tags')
@section('breadcrumb', 'Master Tags')


@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
                <h3 class="card-title">List Tags</h3>
                    <a href="/tags-add"><i class="nav-icon fas fa-plus" style="float: right">ADD</i></a>
                
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th width="120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($tags) && $tags->count())
                        @foreach($tags as $key => $value)
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>
                                    <!-- <button class="btn btn-success" name="tags_id" value="{{ $value->tags_id }}"><i class="nav-icon far fa-edit"></i></button> -->
                                    <a href="/tags-edit/{{ $value->id }}"><i class="nav-icon btn-success fas fa-edit" style="padding: 5px;"></i></a>
                                    <a href="/tags-delete/{{ $value->id }}" onclick="return confirm('Are you sure?')"><i class="nav-icon btn-danger fa fa-trash" style="padding: 5px;"></i></a>
                                    <!-- <button class="btn btn-danger" name="tags_id" value="{{ $value->tags_id }}"><i class="nav-icon fa fa-trash"></i></button> -->
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $tags->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection