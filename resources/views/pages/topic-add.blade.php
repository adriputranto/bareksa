@extends('layouts.master')
@section('title', 'Topic')
@section('page-title', 'Master Topic')
@section('breadcrumb', 'Master Topic')

@section('content')

@php
if($is_edit){
    $disabled = 'disabled';
    $name = $res_edit->name;
    $newsx = explode('|', $res_edit->news_id);
    $action = '/topic-edit/'.$id;
    $id = $id;
}else{
    $disabled = '';
    $name = '';
    $newsx = array();
    $action = '/topic-add';
    $id = '';
}
@endphp
    <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Topic</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ $action }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        @if(!empty($id))
                            <input hidden="" value="{{ $id }}" name="id">
                        @endif
                        
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"  autocomplete="off" value="{{ $name }}">
                    </div>
                    <div class="form-group">
                        <label><strong>Select Tag :</strong></label><br/>
                                                        <select class="form-control" name="news[]" multiple="">
                                                        @foreach($news as $val)
                                                            @php
                                                            if (in_array($val->id, $newsx)) {
                                                                $x = 'selected';
                                                            }else{
                                                                $x='';
                                                            }
                                                            @endphp
                                                          <option {{ $x }} value="{{ $val->id }}">{{ $val->name }}</option>
                                                        @endforeach  
                                                        </select>
                    </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            

          </div>
@endsection