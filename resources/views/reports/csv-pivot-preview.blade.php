@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Preview Pivot Invoice '.strtoupper($region))
@section('breadcrumb', 'Preview Pivot Invoice')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card" style="overflow: scroll;">
              <div class="card-header">
                @can('download')
                <h3 class="card-title">Preview Pivot Invoice</h3>
                @endcan
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>Project Status</th>
                      <th>Price Plan</th>
                      <th>Reseller</th>
                      <th>Project Name</th>
                      <th>VM Name</th>
                      <th>Group</th>
                      <th>Resource Name</th>
                      <th>Status</th>
                      <th>Qty</th>
                      <th>Hour</th>
                      <th>Charged</th>
                      <th>Total Price</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($preview))
                        @php
                        $arrProjectStatus = $arrPricePlan = $arrReseller = $arrProjectName = $arrVmName = $arrGroups = array();
                        @endphp

                        @foreach($preview as $x => $header)
                            @foreach($header as $key => $val)
                                @php
                                $project_status = $price_plan = $reseller = $project_name = $vm_name = $groups = '';
                                if(!in_array($key, $arrProjectStatus)){
                                    $project_status = $key;
                                    $arrProjectStatus[] = $key;
                                }
                                if(!in_array($val['price_plan'], $arrPricePlan)){
                                    $price_plan = $val['price_plan'];
                                    $arrPricePlan[] = $val['price_plan'];
                                }
                                if(!in_array($val['reseller'], $arrReseller)){
                                    $reseller = $val['reseller'];
                                    $arrReseller[] = $val['reseller'];
                                }
                                if(!in_array($val['project_name'], $arrProjectName)){
                                    $project_name = $val['project_name'];
                                    $arrProjectName[] = $val['project_name'];
                                }
                                if(!in_array($val['vm_name'], $arrVmName)){
                                    $vm_name = $val['vm_name'];
                                    $arrVmName[] = $val['vm_name'];
                                    $arrGroups = array();
                                }
                                if(!in_array($val['groups'], $arrGroups)){
                                    $groups = $val['groups'];
                                    $arrGroups[] = $val['groups'];
                                }
                                if($val['charge'])
                                    $charge = 'TRUE';
                                else
                                    $charge = 'FALSE';
                                @endphp
                                <tr>
                                    <td>{{ $project_status }}</td>
                                    <td>{{ $price_plan }}</td>
                                    <td>{{ $reseller }}</td>
                                    <td>{{ $project_name }}</td>
                                    <td>{{ $vm_name }}</td>
                                    <td>{{ $groups }}</td>
                                    <td>{{ $val['resource_name'] }}</td>
                                    <td>{{ $val['status'] }}</td>
                                    <td>{{ $val['qty'] }}</td>
                                    <td>{{ $val['hour'] }}</td>
                                    <td>{{ $charge }}</td>
                                    <td>{{ $val['total'] }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@endpush