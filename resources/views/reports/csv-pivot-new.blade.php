@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'CSV Pivot '.strtoupper($region))
@section('breadcrumb', 'CSV Pivot')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
        <form action="/csv-export-new" method="POST">
        @csrf
              <div class="card-header">
                @can('download')
                <h3 class="card-title">Select Invoice To Download Pivot</h3>
                <select id="region" style="float: right;" class="form-control col-md-2" onchange="changeRegion()" name="region">
                  <option {{ $region=='jk1'?'selected':'' }}>JK1</option>
                  <option {{ $region=='jk2'?'selected':'' }}>JK2</option>
                </select>
                @endcan
              </div>
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Invoice Code</th>
                        <th>Billing Cycle</th>
                        <th width="300px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($invoice) && $invoice->count())
                        @foreach($invoice as $key => $value)
                            <tr>
                                <td><a href="/pivot/{{$region}}/{{ $value->invoice_code }}">{{ $value->invoice_code }}</a></td>
                                <td>{{ $value->billing_cycle }}</td>
                                <td>
                                    <button class="btn btn-danger" name="invoiceCode" value="{{ $value->invoice_code }}"><i class="nav-icon fas fa-download"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $invoice->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        function changeRegion(){
            var region = $('#region').val();
            if(region == 'JK1'){
                window.location.href = '/csv-pivot-jk1';
            }else{
                window.location.href = '/csv-pivot-jk2';
            }
        }
    </script>
@endpush