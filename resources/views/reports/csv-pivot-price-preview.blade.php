@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Preview Pivot Invoice '.strtoupper($region))
@section('breadcrumb', 'Preview Pivot Invoice')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card" style="overflow: scroll;">
              <div class="card-header">
                @can('download')
                <h3 class="card-title">Preview Pivot Invoice</h3>
                @endcan
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>Project Status</th>
                      <th>Price Plan</th>
                      <th>Reseller</th>
                      <th>Project Name</th>
                      <th>Image</th>
                      <th>Instance</th>
                      <th>IP Public</th>
                      <th>License</th>
                      <th>Snapshot</th>
                      <th>Volume</th>
                      <th>Grand Total</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($preview))
                            @foreach($preview as $key => $val)
                                @php
                                $image = $instance = $ip_public = $license = $snapshot = $volume = $grand_total = 0;

                                $project_status = $val['project_status'];
                                $price_plan = $val['price_plan'];
                                $reseller = $val['reseller'];
                                $project_name = $val['project_name'];
                                if(isset($val['image'])){
                                    $image = $val['image'];
                                }
                                if(isset($val['instance'])){
                                    $instance = $val['instance'];
                                }
                                if(isset($val['ip_public'])){
                                    $ip_public = $val['ip_public'];
                                }
                                if(isset($val['license'])){
                                    $license = $val['license'];
                                }
                                if(isset($val['snapshot'])){
                                    $snapshot = $val['snapshot'];
                                }
                                if(isset($val['volume'])){
                                    $volume = $val['volume'];
                                }

                                $grand_total = $image + $instance + $ip_public + $license + $snapshot + $volume;

                                @endphp
                                <tr>
                                    <td>{{ $project_status }}</td>
                                    <td>{{ $price_plan }}</td>
                                    <td>{{ $reseller }}</td>
                                    <td>{{ $project_name }}</td>
                                    <td>{{ $image }}</td>
                                    <td>{{ $instance }}</td>
                                    <td>{{ $ip_public }}</td>
                                    <td>{{ $license }}</td>
                                    <td>{{ $snapshot }}</td>
                                    <td>{{ $volume }}</td>
                                    <td>{{ $grand_total }}</td>
                                    
                                </tr>
                            @endforeach

                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@endpush