@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Preview Invoice '.strtoupper($region))
@section('breadcrumb', 'Preview Invoice')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card" style="overflow: scroll;">
              <div class="card-header">
                @can('download')
                <h3 class="card-title">Preview Invoice</h3>
                <form action="/csv-export-new" method="POST">
                  @csrf
                    <input hidden name="region" value="{{ $region }}">
                    <button style="font-size: 10px; float: right;" class="btn btn-danger" name="invoiceCode" value="{{ $invoiceCode }}"><i class="nav-icon fas fa-download"></i></button>
                </form>
                @endcan
              </div>
        <form action="/csv-export-new" method="POST">
          @csrf
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Site</th>
                      <th>Billing Cycle</th>
                      <th>Reseller</th>
                      <th>Reseller ID</th>
                      <th>Project</th>
                      <th>Project ID</th>
                      <th>Service</th>
                      <th>Resource Type</th>
                      <th>Resource ID</th>
                      <th>Resource Name</th>
                      <th>Parent ID</th>
                      <th>Parent Name</th>
                      <th>Status</th>
                      <th>OS</th>
                      <th>Qty</th>
                      <th>Hour</th>
                      <th>Project Name</th>
                      <th>Reseller</th>
                      <th>Price Plan</th>
                      <th>Project Status</th>
                      <th>Group</th>
                      <th>VM ID</th>
                      <th>VM Name</th>
                      <th>vCPU</th>
                      <th>vRAM</th>
                      <th>Charge</th>
                      <th>Unit Price</th>
                      <th>SQL Price</th>
                      <th>Total Price</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($preview))
                        @foreach($preview as $key => $value)
                            <tr>
                                <td>{{ $value->sites }}</td>
                                <td>{{ $value->billing_cycle }}</td>
                                <td>{{ $value->reseller_name }}</td>
                                <td>{{ $value->reseller_id }}</td>
                                <td>{{ $value->project_name }}</td>
                                <td>{{ $value->project_id }}</td>
                                <td>{{ $value->service }}</td>
                                <td>{{ $value->resource_type }}</td>
                                <td>{{ $value->resource_id }}</td>
                                <td>{{ $value->resource_name }}</td>
                                <td>{{ $value->parent_id }}</td>
                                <td>{{ $value->parent_name }}</td>
                                <td>{{ $value->status }}</td>
                                <td>{{ $value->os }}</td>
                                <td>{{ $value->qty }}</td>
                                <td>{{ $value->hour }}</td>
                                <td>{{ $value->project_name }}</td>
                                <td>{{ $value->reseller }}</td>
                                <td>{{ $value->price_plan }}</td>
                                <td>{{ $value->project_status }}</td>
                                <td>{{ $value->group }}</td>
                                <td>{{ $value->vmid }}</td>
                                <td>{{ $value->vmname }}</td>
                                <td>{{ $value->vcpu }}</td>
                                <td>{{ $value->ram }}</td>
                                <td>{{ $value->charge }}</td>
                                <td>{{ $value->unit_price }}</td>
                                <td>{{ $value->sql_price }}</td>
                                <td>{{ $value->total_price }}</td>
                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $preview->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@endpush