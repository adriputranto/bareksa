@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'CSV Download')
@section('breadcrumb', 'CSV Download')
@push('custom-css')
<style>
  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }
  
  .tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
  
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
  }
  
  .tooltip:hover .tooltiptext {
    visibility: visible;
  }
</style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                @can('download')
                <h3 class="card-title">Select Invoice To Download</h3>
                @endcan
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="invoices" class="table table-bordered table-hover display nowrap">
                  <thead>
                    <tr>
                      <th>Invoice Code</th>
                      <th>Billing Cycle</th>
                      <th>Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- <tr>
                      <td>01/01/2021 - 31/01/2021</td>
                      <td><a href="#"><i class="nav-icon fas fa-download"></i></a></td>
                    </tr> --}}
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Invoice Code</th>
                      <th>Billing Cycle</th>
                      <th>Download</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Preview Invoice Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="invoicedetails" class="table table-bordered table-striped display nowrap">
                  <thead>
                    <tr>
                      <th>Site</th>
                      <th>Billing Cycle</th>
                      <th>Reseller</th>
                      <th>Reseller ID</th>
                      <th>Project</th>
                      <th>Project ID</th>
                      <th>Service</th>
                      <th>Resource Type</th>
                      <th>Resource ID</th>
                      <th>Resource Name</th>
                      <th>Parent ID</th>
                      <th>Parent Name</th>
                      <th>Status</th>
                      <th>OS</th>
                      <th>Qty</th>
                      <th>Hour</th>
                      <th>Project Name</th>
                      <th>Reseller</th>
                      <th>Price Plan</th>
                      <th>Project Status</th>
                      <th>Group</th>
                      <th>VM ID</th>
                      <th>VM Name</th>
                      <th>vCPU</th>
                      <th>vRAM</th>
                      <th>Charge</th>
                      <th>Unit Price</th>
                      <th>SQL Price</th>
                      <th>Total Price</th>
                    </tr>
                  </thead>
                  <tbody>
                  {{-- <tr>
                    <td>JK2</td>
                    <td>01/10/2020 - 31/10/2020</td>
                    <td>Elitery Reseller</td>
                    <td>485541</td>
                    <td>Elitery-ELIBSC001</td>
                    <td>c530e0d6f620408c98ecc32c7be532f8</td>
                    <td>network</td>
                    <td>router_ip</td>
                    <td>9b99637e-0bbf-402b-9b09-c309127255d6</td>
                    <td>43.241.148.95</td>
                    <td>9b99637e-0bbf-402b-9b09-c309127255d6</td>
                    <td>ELIBSC001-gw</td>
                    <td>active</td>
                    <td>-</td>
                    <td>1</td>
                    <td>744</td>
                    <td>69.44</td>
                    <td>51663.36</td>
                  </tr> --}}
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Site</th>
                      <th>Billing Cycle</th>
                      <th>Reseller</th>
                      <th>Reseller ID</th>
                      <th>Project</th>
                      <th>Project ID</th>
                      <th>Service</th>
                      <th>Resource Type</th>
                      <th>Resource ID</th>
                      <th>Resource Name</th>
                      <th>Parent ID</th>
                      <th>Parent Name</th>
                      <th>Status</th>
                      <th>OS</th>
                      <th>Qty</th>
                      <th>Hour</th>
                      <th>Project Name</th>
                      <th>Reseller</th>
                      <th>Price Plan</th>
                      <th>Project Status</th>
                      <th>Group</th>
                      <th>VM ID</th>
                      <th>VM Name</th>
                      <th>vCPU</th>
                      <th>vRAM</th>
                      <th>Charge</th>
                      <th>Unit Price</th>
                      <th>SQL Price</th>
                      <th>Total Price</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(document).ajaxStart(function() {
            $(document.body).css({'cursor' : 'wait'});
        }).ajaxStop(function() {
            $(document.body).css({'cursor' : 'default'});
        });

        $(document).ready(function() {
          // $.fn.dataTable.ext.errMode = 'throw';

          // LOAD INVOICES (BILLING CYCLE)
          $('#invoices').DataTable({
            processing: true,
            language: {
              processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading</span>'
            },
            serverSide: true,
            ajax: {
              url: "{{ action('InvoicesController@InvoicesList') }}", 
              method: "get"
            },
            autoWidth: false,
            pageLength: 3,
            lengthMenu: [3, 5, 10],
            error: function(x, y){
              console.log(x);
              debugger;
            },
            columns: [
              {data: 'invoice_code', name: 'invoice_code'},
              {data: 'billing_cycle', name: 'billing_cycle'},
              {
                  data: 'action', 
                  name: 'action', 
                  orderable: true, 
                  searchable: true,
                  className: 'dt-center'
              },
            ]
          });
          
          // ENABLE HORIZONTAL SCROLLBAR ON INVOICE DETAILS
          $('#invoicedetails').DataTable( {
            destroy: true,
            autoWidth: false,
            scrollX: true
          } );
          
          // ON INVOICES LIST CLICK DOWNLOAD TO CSV
          var table = $('#invoices').DataTable();
          $('#invoices tbody').on( 'click', 'button', function() {
            var invoiceCode = table.row( $(this).parents('tr') ).data()['invoice_code'];
            $.ajax({
              method: "GET",
              url: "{{ action('InvoicesController@ExportCsv') }}",
              data: {invoiceCode:invoiceCode},
              success: function (response, respCode, headers) {
                var FileName = headers.getResponseHeader("Filename");
                
                /*
                * Make CSV downloadable
                */
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+response];

                var blobObject = new Blob(fileData,{
                  type: "text/csv;charset=utf-8;"
                });

                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = FileName;

                /*
                * Actually download CSV
                */
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
              }
            } );
          } );

          // ON INVOICES ROW CLICK SHOW INVOICE DETAILS DATA
          var selectedInvoice = $('#invoices').DataTable();
          $('#invoices tbody').on( 'click', 'tr', function () {
            var invoiceCode = selectedInvoice.row(this).data()['invoice_code'];
            // alert(invoiceCode);

            $('#invoicedetails').DataTable({
              destroy: true,
              processing: true,
              language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading</span>'
              },
              serverSide: true,
              ajax: {
                url: "{{ action('InvoicesController@InvoiceDataList') }}", 
                method: "get",
                data: {invoiceCode:invoiceCode}
              },
              error: function(x, y){
                console.log(x);
                debugger;
              },
              autoWidth: false,
              columns: [
                {data: 'sites', name: 'sites'},
                {data: 'billing_cycle', name: 'billing_cycle'},
                {data: 'reseller_name', name: 'reseller_name'},
                {data: 'reseller_id', name: 'reseller_id'},
                {data: 'project_name', name: 'project_name'},
                {data: 'project_id', name: 'project_id'},
                {data: 'service', name: 'service'},
                {data: 'resource_type', name: 'resource_type'},
                {data: 'resource_id', name: 'resource_id'},
                {data: 'resource_name', name: 'resource_name'},
                {data: 'parent_id', name: 'parent_id'},
                {data: 'parent_name', name: 'parent_name'},
                {data: 'status', name: 'status'},
                {data: 'os', name: 'os'},
                {data: 'qty', name: 'qty'},
                {data: 'hour', name: 'hour'},
                {data: 'project_name', name: 'project_name'},
                {data: 'reseller', name: 'reseller'},
                {data: 'price_plan', name: 'price_plan'},
                {data: 'project_status', name: 'project_status'},
                {data: 'group', name: 'group'},
                {data: 'vmid', name: 'vmid'},
                {data: 'vmname', name: 'vmname'},
                {data: 'vcpu', name: 'vcpu'},
                {data: 'ram', name: 'ram'},
                {data: 'charge', name: 'charge'},
                {data: 'unit_price', name: 'unit_price'},
                {data: 'sql_price', name: 'sql_price'},
                {data: 'total_price', name: 'total_price'},
              ],
              scrollX: true
            } );
          } );          
        } );
    </script>
@endpush