@extends('layouts.master')
@section('title', Config::get('fleio.title'))
@section('page-title', 'Monthly Usage')
@section('breadcrumb', 'Monthly Usage')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
        <form action="/export-invoice" method="POST">
        <!-- <form action="/csv-export-new" method="POST"> -->
        @csrf
              <div class="card-header">
                <h3 class="card-title">Select Usage Report To Download</h3>
                @can('download')
                
                @endcan
              </div>
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Usage ID</th>
                        <th>Billing Cycle</th>
                        <th>Reseller Group</th>
                        <th width="300px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($invoice) && $invoice->count())
                        @foreach($invoice as $key => $value)
                            <?php 
                                if(empty($value->reseller_group)){
                                    continue;
                                }
                            ?>
                            <tr>
                                <td>{{ $value->invoice_code }}</td>
                                <td>{{ $value->billing_cycle }}</td>
                                <td>{{ $value->reseller_group }}</td>
                                <td>
                                    <!-- <input hidden name='reseller_id' value="{{ $value->reseller_id }}"> -->
                                    <button class="btn btn-danger" name="invoiceCode" value="{{ $value->invoice_code.'-'.$value->reseller_group }}"><i class="nav-icon fas fa-download"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">There are no data.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
    {!! $invoice->links() !!}
  </form>

            </div>
            <!-- /.card -->

  
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
@endsection

@push('custom-js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('') }}assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/jszip/jszip.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('') }}assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        function changeRegion(){
            var region = $('#region').val();
            if(region == 'JK1'){
                window.location.href = '/csv-download-jk1';
            }else{
                window.location.href = '/csv-download-jk2';
            }
        }
    </script>
@endpush