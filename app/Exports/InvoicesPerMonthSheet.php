<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Models\Invoices;
use App\Models\Acronis;
use App\Models\OpenPort;

class InvoicesPerMonthSheet implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $invoice_code;
    private $reseller_group;
    private $title;
    private $pivotPrice;

    public function __construct(int $invoice_code, string $reseller_group, string $title, array $pivotPrice)
    {
        $this->invoice_code = $invoice_code;
        $this->reseller_group  = $reseller_group;
        $this->title  = $title;
        $this->pivotPrice = $pivotPrice;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        $month = array('01' => 'Jan','02' => 'Feb','03' => 'Mar','04' => 'Apr','05' => 'May','06' => 'Jun','07' => 'Jul','08' => 'Aug','09' => 'Sep','10' => 'Oct','11' => 'Nov','12' => 'Dec',);
        $prdMonth =$month[substr($this->invoice_code, -2)];
        $prdYear ='20'.substr($this->invoice_code, 0, 2);
        $period = $prdMonth.'-'.$prdYear;

        if($this->title == 'jk1' || $this->title == 'jk2'){
            $query = Invoices::query()->where('invoice_code', $this->invoice_code);
            if(!empty($this->reseller_group)){
                $query->where('reseller_group', $this->reseller_group);
            }
            $query->where('site', $this->title);    
        }
        if($this->title == 'acronis') {
            $query = Acronis::query()->where('period', $period);

        }
        if($this->title == 'openport') {
            $query = OpenPort::query()->with(['reseller','project'])->where('period', $period);
        }
        if($this->title == 'usage jk1' || $this->title == 'usage jk2'){
            $query = Invoices::query()->from('invoices as a')->select('a.project_status','a.reseller','a.project_id','a.project_name','a.invoice_code','a.site','c.price_plan', 'a.vm_name', 'a.groups', 'a.resource_name', 'a.status', 'a.qty', 'a.hour', 'a.charge', 'a.total')    
                    ->join('project as c', function($join) {
                        $join->on('a.project_id', '=', 'c.project_id');
                    })
                    ->where('a.site', substr($this->title, -3))
                    ->where('a.invoice_code', $this->invoice_code);
                    if(!empty($this->reseller_group)){
                        $query->where('a.reseller_group', $this->reseller_group);
                    }
                    $query->whereIn('a.project_status', ['POC','Production'])
                    ->orderBy('a.project_status')
                    ->orderBy('c.price_plan')
                    ->orderBy('a.project_name')
                    ->orderBy('a.vm_name')
                    ->orderBy('a.groups')
                    ->groupBy('a.project_status','a.reseller','a.project_id','a.project_name','a.invoice_code','a.site','c.price_plan','a.vm_name','a.groups', 'a.resource_name', 'a.status', 'a.qty', 'a.hour', 'a.charge', 'a.total');
        }
        if($this->title == 'pivot jk1' || $this->title == 'pivot jk2'){
            $query = Invoices::query()->select("project_name")
                    ->whereIn('project_status', ['POC','Production'])
                    ->where('site', substr($this->title, -3))
                    ->where('invoice_code', $this->invoice_code);
                    if(!empty($this->reseller_group)){
                        $query->where('reseller_group', $this->reseller_group);
                    }
                    $query->orderBy('project_status')
                    ->orderBy('project_name')
                    ->groupBy('project_status','project_name');
        }

        if($this->title == 'summary usage'){
            $queryc = Invoices::query()->selectRaw('\'vRAM\' as resources, SUM(vram::integer) as allocated')->
            where('vram','!=','')->groupBy('resources')->where('invoice_code', '2101');

            $queryb = Invoices::query()->selectRaw('\'vCPU\' as resources, SUM(vcpu::integer) as allocated')->
            where('vcpu','!=','')->groupBy('resources')->where('invoice_code', '2101');

            $querya = Invoices::query()->selectRaw('resource_type as resources, SUM(qty) as allocated')->
            whereIn('resource_type',['Storage Tier 1', 'Storage Tier 2', 'Storage Tier 3'])->groupBy('resource_type')->where('invoice_code', '2101')->orderBy('resource_type');

            $query = $querya->union($queryb)->union($queryc);
            // dd($query);

        }

        if($this->title == 'sql usage'){
            $query = Invoices::query()->where('invoice_code', '9999');
        }
        
        return $query;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return ucwords($this->title);
    }

    public function headings(): array
    {
        if($this->title == 'jk1' || $this->title == 'jk2'){
            return [
                "Invoice Code",
                "Rev",
                "Site",
                "Billing Cycle",
                "Reseller",
                "Reseller ID",
                "Project",
                "Project ID",
                "Service",
                "Resource Type",
                "Resource ID",
                "Resource Name",
                "Parent ID",
                "Parent Name",
                "Status",
                "OS",
                "Qty",
                "Hour",
                "Project Name",
                "Price Plan",
                "Project Status",
                "Groups",
                "VM ID",
                "VM Name",
                "vCPU",
                "vRAM",
                "Charge",
                "Unit Price",
                "SQL Price",
                "Total",
            ];
        }
        if($this->title == 'acronis') {
            return [
                "Period",
                "Partner",
                "Customer",
                "Site",
                "Status",
                "Price Plan",
                "Item",
                "Usage",
                "Pricing",
                "Total",
            ];
        }
        if($this->title == 'openport') {
            return [
                "Period",
                "Site",
                "Partner",
                "Customer",
                "Project",
                "Service",
                "Qty",
                "Speed",
                "Start Date",
                "Description",
            ];
        }
        if($this->title == 'usage jk1' || $this->title == 'usage jk2') {
            return [
                "Project Status",
                "Reseller",
                "Project Name",
                "Invoice Code",
                "Site",
                "Price Plan",
                "VM Name",
                "Groups",
                "Resource Name",
                "Status",
                "Qty",
                "Hour",
                "Charge",
                "Total",
            ];
        }
        if($this->title == 'pivot jk1' || $this->title == 'pivot jk2'){
            return ["Project Status",
                    "Price Plan",
                    "Reseller",
                    "Project Name",
                    "Image",
                    "Instance",
                    "IP Public",
                    "License",
                    "Snapshot",
                    "Volume",
                    "Total",
                   ];
        }
        if($this->title == 'summary usage'){
            return ["Resources","Allocated"];
        }
        if($this->title == 'sql usage'){
            return [];
        }
    }

    public function map($invoice): array
    {
        if($this->title == 'acronis') {
            $arrayTemp = array();
            foreach(json_decode($invoice->item) as $i => $val){
                if($i == 0) {
                    $arrayTemp[$i] = array(
                        $invoice->period,
                        $invoice->partner,
                        $invoice->customer,
                        $invoice->site,
                        $invoice->status,
                        $invoice->price_plan,
                        $val->item,
                        $val->usage,
                        (empty($val->pricing)) ? $val->pricing : number_format($val->pricing, 2),
                        (empty($val->total)) ? $val->total : number_format($val->total, 2)
                    ); 
                } else {
                    $arrayTemp[$i] = array(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        $val->item,
                        $val->usage,
                        (empty($val->pricing)) ? $val->pricing : number_format($val->pricing, 2),
                        (empty($val->total)) ? $val->total : number_format($val->total, 2)
                    );
                }
            }

            return $arrayTemp;
        } 

        if($this->title == 'openport') {
            // dd($invoice->reseller->reseller_name);
            if(!isset($invoice->reseller->reseller_name)){
                return [];
            }
            return [
                $invoice->period,
                $invoice->site,
                $invoice->reseller->reseller_name,
                $invoice->customer,
                $invoice->project->project_name,
                $invoice->service,
                $invoice->qty,
                $invoice->speed,
                $invoice->start_date,
                $invoice->description,
            ];
        }
        if($this->title == 'jk1' || $this->title == 'jk2'){
            return [
                $invoice->invoice_code,
                $invoice->rev,
                $invoice->site,
                $invoice->billing_cycle,
                $invoice->reseller,
                $invoice->reseller_id,
                $invoice->project,
                $invoice->project_id,
                $invoice->service,
                $invoice->resource_type,
                $invoice->resource_id,
                $invoice->resource_name,
                $invoice->parent_id,
                $invoice->parent_name,
                $invoice->status,
                $invoice->os,
                $invoice->qty,
                $invoice->hour,
                $invoice->project_name,
                $invoice->price_plan,
                $invoice->project_status,
                $invoice->groups,
                $invoice->vm_id,
                $invoice->vm_name,
                $invoice->vcpu,
                $invoice->vram,
                ($invoice->charge == 1)? 'TRUE' : 'FALSE',
                // $invoice->unit_price,
                (empty($invoice->unit_price)) ? $invoice->unit_price : number_format($invoice->unit_price, 2),
                $invoice->sql_price,
                (empty($invoice->total)) ? $invoice->total : number_format($invoice->total, 2)
            ];
        }
        if($this->title == 'usage jk1' || $this->title == 'usage jk2') {
            return [
                $invoice->project_status,
                $invoice->reseller,
                $invoice->project_name,
                $invoice->invoice_code,
                $invoice->site,
                $invoice->price_plan,
                $invoice->vm_name,
                $invoice->groups,
                $invoice->resource_name,
                $invoice->status,
                $invoice->qty,
                $invoice->hour,
                $invoice->charge,
                (empty($invoice->total)) ? $invoice->total : number_format($invoice->total, 2)
            ];
        }
        if($this->title == 'pivot jk1' || $this->title == 'pivot jk2'){
            // dd($invoice);
            // dd($this->pivotPrice);
            if(isset($this->pivotPrice[$invoice->project_name])){
                $data = $this->pivotPrice[$invoice->project_name];
                    // dd($data['instance']);
                    $image = $instance = $ip_public = $license = $snapshot = $volume = $total = '';
                    if(isset($data['image'])){ $image = $data['image']; }
                    if(isset($data['instance'])){ $instance = $data['instance']; }
                    if(isset($data['ip_public'])){ $ip_public = $data['ip_public']; }
                    if(isset($data['license'])){ $license = $data['license']; }
                    if(isset($data['snapshot'])){ $snapshot = $data['snapshot']; }
                    if(isset($data['volume'])){ $volume = $data['volume']; }
                    if(isset($data['total'])){ $total = $data['total']; }
                return [
                    $data['project_status'],
                    $data['price_plan'],
                    $data['reseller'],
                    $data['project_name'],
                    (empty($image)) ? $image : number_format($image, 2),
                    (empty($instance)) ? $instance : number_format($instance, 2),
                    (empty($ip_public)) ? $ip_public : number_format($ip_public, 2),
                    (empty($license)) ? $license : number_format($license, 2),
                    (empty($snapshot)) ? $snapshot : number_format($snapshot, 2),
                    (empty($volume)) ? $volume : number_format($volume, 2),
                    (empty($total)) ? $total : number_format($total, 2)
                ];
            }else{
                return [];
            }
        }
        if($this->title == 'summary usage'){
            // dd($invoice);
            return [
                $invoice->resources,
                $invoice->allocated
            ];
        }
        if($this->title == 'sql usage'){
            return [];
        }
    }
}