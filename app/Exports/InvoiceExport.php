<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\InvoicesPerMonthSheet;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Invoices;
use Illuminate\Support\Facades\Auth;

class InvoiceExport implements WithMultipleSheets, FromCollection
{
    use Exportable;

    protected $invoice_code;
    protected $reseller_group;
    protected $pivotPrice;
    
    public function __construct(int $invoice_code, string $reseller_group, array $pivotPrice)
    {
        $this->invoice_code = $invoice_code;
        $this->reseller_group = $reseller_group;
        $this->pivotPrice = $pivotPrice;
    }

    public function collection()
    {
        return Invoices::all();
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $title [] = 'jk1';
        $title [] = 'jk2';
        $title [] = 'usage jk1';
        $title [] = 'usage jk2';
        $title [] = 'acronis';
        $title [] = 'openport';
        $title [] = 'pivot jk1';
        $title [] = 'pivot jk2';
        if(Auth::user()->role == 'admin'){
            $title [] = 'summary usage';
            $title [] = 'sql usage';
        }

        foreach ($title as $ttl) {
            $sheets[] = new InvoicesPerMonthSheet($this->invoice_code, $this->reseller_group, $ttl, $this->pivotPrice);
        }

        return $sheets;
    }

}