<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;
    public $connection = 'pgsql';
    protected $table = 'topic';
    protected $fillable = ['name', 'news_id'];
        
            public function getAllDataWithPaginate(){
                $result = $this->paginate(10);
                return $result;
            }
}
