<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    public $connection = 'pgsql';
    protected $table = 'news';
    protected $fillable = ['name', 'tag_id', 'status'];

    public function getAllDataWithPaginate($filter){
        if(!empty($filter)){
            $result = $this->where('status', $filter)->paginate(10);
        }else{
            $result = $this->paginate(10);
        }
        return $result;
    }

    public function getNewsName($arrId = null){
        $temp = explode('|',$arrId);
        $temp2 = $this->select('name')->whereIn('id', $temp)->get();
        $temp3 =array();
        foreach($temp2 as $val){
            $temp3[] = $val->name;
        }
        $result = implode(', ',$temp3);
        return $result;
    }

    // public function quotation()
    // {
    //     return $this->belongsTo(QuotationHeader::class);
    // }
}
