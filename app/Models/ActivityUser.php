<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityUser extends Model
{
    use HasFactory;
    public $connection = 'pgsql';
    protected $table = 'activity_user';
    protected $fillable = ['user_id', 'action'];
        
            public function getAllDataWithPaginate(){
                $result = $this->paginate(10);
                return $result;
            }
}
