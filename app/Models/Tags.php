<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;
    public $connection = 'pgsql';
    protected $table = 'tags';
    protected $fillable = ['name'];

    public function getAllDataWithPaginate(){
        $result = $this->paginate(10);
        return $result;
    }

    public function getTagName($arrId = null){
        $temp = explode('|',$arrId);
        $temp2 = $this->select('name')->whereIn('id', $temp)->get();
        $temp3 =array();
        foreach($temp2 as $val){
            $temp3[] = $val->name;
        }
        $result = implode(', ',$temp3);
        return $result;
    }

    public function quotation_detail()
    {
        return $this->belongsTo(QuotationDetail::class);
    }
}
