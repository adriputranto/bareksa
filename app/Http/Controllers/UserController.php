<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    public function user() {
        $user = new User();
        $data=array();
        $user = $user->paginate(10);
        $data['user'] = $user;
        return view('pages.user', $data);
    }

    public function user_add() {
        $data=array();

        $data['is_edit'] = false;

        return view('pages.user-add', $data);
    }

    public function user_edit($id) {
        $res = User::find($id);
        $data=array();
        $data['res_edit'] = $res;
        $data['is_edit'] = true;
        $data['id'] = $id;

        return view('pages.user-add', $data);
    }

    public function user_store(Request $request) {
        $req = $request->all();

        $user = new User();

        $user->name = $req['name'];
        $user->email = $req['email'];
        $user->password = Hash::make($req['password']);
        $user->role = $req['role'];
        $user->created_at = date('Y-m-d H:i:s');
        $user->updated_at = date('Y-m-d H:i:s');
        
        $user->save();

        //record activity user
        Controller::recordActivityUser('Add new user '.$req['name']);

        return redirect('/user');
    }

    public function user_store_edit(Request $request) {
        $req = $request->all();

        array_shift($req);
        $user = User::where('id',$request->id)->first();

        $user->name = $req['name'];
        $user->email = $req['email'];
        if(!empty($req['password'])){
            $user->password = Hash::make($req['password']);
        }
        $user->role = $req['role'];
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();

        //record activity user
        Controller::recordActivityUser('Edit data user '.$req['name']);

        return redirect('/user');
    }

    public function user_delete($id) {
        $user = User::where('id',$id)->delete();
        //record activity user
        Controller::recordActivityUser('Delete data user '.$id);
        return redirect('/user');
    }

}
