<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tags;

class TagsController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function tags() {
        $tags = new Tags();
        $data=array();
        $data['tags'] = $tags->getAllDataWithPaginate();
        return view('pages.tags', $data);
    }

    public function tags_add() {
        $project = new Tags();
        $tags = Tags::all();
        $data=array();
        $data['is_edit'] = false;
        $data['tags'] = $tags;

        return view('pages.tags-add', $data);
    }

    public function tags_edit($id) {
        $res = Tags::find($id);
        $tags = Tags::all();
        $data=array();
        $data['res_edit'] = $res;
        $data['is_edit'] = true;
        $data['id'] = $id;
        $data['tags'] = $tags;
        return view('pages.tags-add', $data);
    }

    public function tags_store(Request $request) {
        $req = $request->all();

        $req['created_at'] = date('Y-m-d H:i:s');
        $req['updated_at'] = date('Y-m-d H:i:s');
        
        array_shift($req);
        Tags::create($req);

        return redirect('/tags');
    }

    public function tags_store_edit(Request $request) {
        $req = $request->all();
        array_shift($req);
        $tags = Tags::where('id',$request->id)->first();

        $tags->name = $req['name'];
        $tags->updated_at = date('Y-m-d H:i:s');

        $tags->save();

        return redirect('/tags');
    }

    public function tags_delete($id) {
        $tags = Tags::where('id',$id)->delete();
        
        return redirect('/tags');
    }
}
