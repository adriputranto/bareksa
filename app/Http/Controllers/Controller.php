<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\ActivityUser;
use App\Models\ActivityLog;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function recordActivityUser($action){
        $activity_user = new ActivityUser();
        $activity_user->user_id = Auth::user()->id;
        $activity_user->action = $action;
        $activity_user->created_at = date('Y-m-d H:i:s');
        $activity_user->updated_at = date('Y-m-d H:i:s');
        $activity_user->save();
    }

    public static function recordActivityLog($site, $status, $action, $message){
        $activity_log = new ActivityLog();
        $activity_log->site = $site;
        $activity_log->status = $status;
        $activity_log->action = $action;
        $activity_log->message = substr($message, 0, 255);
        $activity_log->created_at = date('Y-m-d H:i:s');
        $activity_log->updated_at = date('Y-m-d H:i:s');
        $activity_log->save();
    }
}
