<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\News;
use App\Models\Tags;

class NewsController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(Request $request)
    {

        $filter = $request->all();
        if(isset($filter['filter'])){
            $users = News::where('name', 'LIKE', '%'.$filter['filter'].'%')->get();
        } else {
            $users = News::all();
        }
        return response()->json( $users );
    }
    
    public function news(Request $request) {
        $search =  $request->input('q');
        $news = new News();
        $data=array();
        $data['news'] = $news->getAllDataWithPaginate($search);
        return view('pages.news', $data);
    }

    public function news_add() {
        $project = new News();
        $tags = Tags::all();
        $data=array();
        $data['is_edit'] = false;
        $data['tags'] = $tags;

        return view('pages.news-add', $data);
    }

    public function news_edit($id) {
        $res = News::find($id);
        $tags = Tags::all();
        $data=array();
        $data['res_edit'] = $res;
        $data['is_edit'] = true;
        $data['id'] = $id;
        $data['tags'] = $tags;
        return view('pages.news-add', $data);
    }

    public function news_store(Request $request) {
        $req = $request->all();

        $req['tag_id'] = implode('|', $req['tag']);
        $req['status'] = $req['status'];
        $req['created_at'] = date('Y-m-d H:i:s');
        $req['updated_at'] = date('Y-m-d H:i:s');
        
        array_shift($req);
        News::create($req);

        return redirect('/news');
    }

    public function news_store_edit(Request $request) {
        $req = $request->all();
        array_shift($req);
        $news = News::where('id',$request->id)->first();

        $news->name = $req['name'];
        $news->status = $req['status'];
        $news->tag_id = implode('|', $req['tag']);
        $news->updated_at = date('Y-m-d H:i:s');

        $news->save();

        return redirect('/news');
    }

    public function news_delete($id) {
        $news = News::where('id',$id)->delete();
        
        return redirect('/news');
    }
}
