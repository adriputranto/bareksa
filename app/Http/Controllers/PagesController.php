<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\csv_invoices;
use App\Models\csv_invoices_jk1;
use App\Models\Invoices;

class PagesController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function Dashboard() {
        return view('dashboard');
    }

    public function CSVDownload() {
        return view('reports.csv-download');
    }

    // public function CSVDownloadNew() {
    //     $endpath = request()->segment(count(request()->segments()));
    //     $region = substr($endpath, -3);
    //     if($region == 'new'){
    //         $region = 'jk1';
    //     }

    //     $can_access = false;
    //     if(Auth::user()->role == 'admin'){
    //         $can_access = true;
    //     }

    //     $data = array();
    //     $data['region'] = $region;
    //     if($region == 'jk1'){
    //         $data['invoice'] = csv_invoices_jk1::select('invoice_code','billing_cycle','reseller_id','reseller_name')->orderBy('invoice_code')
    //             ->groupBy('invoice_code','billing_cycle','reseller_id','reseller_name')->paginate(10);
    //     }else{
    //         $data['invoice'] = csv_invoices::select('invoice_code','billing_cycle','reseller_id','reseller_name')->orderBy('invoice_code')
    //             ->groupBy('invoice_code','billing_cycle','reseller_id','reseller_name')->paginate(10);
    //     }

    //     return view('reports.csv-download-new', $data);
    // }

    public function CSVDownloadNew() {
        $invoices = new Invoices();
        $data=array();
        if(Auth::user()->role == 'reseller'){
            // $reseller = Reseller::where('reseller_id', Auth::user()->reseller_id)->first();
            $invoices = $invoices->select('invoice_code', 'billing_cycle','reseller_group')->where('reseller_group', Auth::user()->reseller_group)->groupBy('invoice_code')->groupBy('billing_cycle')->groupBy('reseller_group');
        } else {
            $invoices = $invoices->select('invoice_code', 'billing_cycle','reseller_group')->groupBy('invoice_code')->groupBy('billing_cycle')->groupBy('reseller_group');
            
            $invoicesb = Invoices::selectRaw('invoice_code, billing_cycle, \'ALL\' as reseller_group')->groupBy('invoice_code')->groupBy('billing_cycle')->groupBy('reseller_group');
            $invoices = $invoices->union($invoicesb);
        }
        $invoices = $invoices->orderBy('invoice_code', 'DESC');
        $invoices = $invoices->orderBy('reseller_group');
        $invoices = $invoices->paginate(10);
        $data['invoice'] = $invoices;
        return view('reports.csv-download-new', $data);
    }

    public function CSVPivotNew() {
        $endpath = request()->segment(count(request()->segments()));
        $region = substr($endpath, -3);
        if($region == 'new'){
            $region = 'jk1';
        }

        $can_access = false;
        if(Auth::user()->role == 'admin'){
            $can_access = true;
        }

        $data = array();
        $data['region'] = $region;
        if($region == 'jk1'){
            $data['invoice'] = csv_invoices_jk1::select('invoice_code','billing_cycle')->orderBy('invoice_code')
                ->groupBy('invoice_code','billing_cycle')->paginate(3);
        }else{
            $data['invoice'] = csv_invoices::select('invoice_code','billing_cycle')->orderBy('invoice_code')
                ->groupBy('invoice_code','billing_cycle')->paginate(3);
        }

        return view('reports.csv-pivot-new', $data);
    }
}
