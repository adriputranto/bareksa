<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\News;
use App\Models\Topic;

class TopicController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function topic() {
        $topic = new Topic();
        $data=array();
        $data['topic'] = $topic->getAllDataWithPaginate();
        return view('pages.topic', $data);
    }

    public function topic_add() {
        $project = new Topic();
        $news = News::all();
        $data=array();
        $data['is_edit'] = false;
        $data['news'] = $news;

        return view('pages.topic-add', $data);
    }

    public function topic_edit($id) {
        $res = Topic::find($id);
        $news = News::all();
        $data=array();
        $data['res_edit'] = $res;
        $data['is_edit'] = true;
        $data['id'] = $id;
        $data['news'] = $news;
        return view('pages.topic-add', $data);
    }

    public function topic_store(Request $request) {
        $req = $request->all();

        $req['news_id'] = implode('|', $req['news']);
        $req['created_at'] = date('Y-m-d H:i:s');
        $req['updated_at'] = date('Y-m-d H:i:s');
        
        array_shift($req);
        Topic::create($req);

        return redirect('/topic');
    }

    public function topic_store_edit(Request $request) {
        $req = $request->all();
        array_shift($req);
        $topic = Topic::where('id',$request->id)->first();

        $topic->name = $req['name'];
        $topic->news_id = implode('|', $req['news']);
        $topic->updated_at = date('Y-m-d H:i:s');

        $topic->save();

        return redirect('/topic');
    }

    public function topic_delete($id) {
        $topic = Topic::where('id',$id)->delete();
        
        return redirect('/topic');
    }
}
